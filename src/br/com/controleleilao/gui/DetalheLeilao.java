package br.com.controleleilao.gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import br.com.controleleilao.negocio.Bem;
import br.com.controleleilao.negocio.BemException;
import br.com.controleleilao.negocio.Leilao;
import br.com.controleleilao.negocio.LeilaoException;
import br.com.controleleilao.negocio.LeilaoFachada;
import br.com.controleleilao.negocio.Lote;
import br.com.controleleilao.negocio.LoteException;
import br.com.controleleilao.negocio.Usuario;
import br.com.controleleilao.negocio.UsuarioException;
import br.com.controleleilao.negocio.UsuarioFachada;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JComboBox;

public class DetalheLeilao extends JPanel {

	private LeilaoFachada leilaoFachada = new LeilaoFachada();
	private JTextField textMinutoInicio;
	private JTextField textHoraInicio;
	private JTextField textDiaInicio;
	private JTextField textMesInicio;
	private JTextField textAnoInicio;
	private JTextField textAnoFim;
	private JTextField textMesFim;
	private JTextField textDiaFim;
	private JTextField textHoraFim;
	private JTextField textMinutoFim;
	private JTable tableLotes;
	
	public DetalheLeilao(ControleTelaJFrame frame, Leilao leilao) {
		super();
		setBounds(100, 100, 614, 487);
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setLayout(null);
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(48, 181, 500, 219);
		add(scrollPane);
		
		
		Object[] colunas = {"Valor", "Bens"};
		DefaultTableModel tableModel = new DefaultTableModel(colunas, 0);
		tableModel.setColumnIdentifiers(colunas);
		tableLotes = new JTable(tableModel);
		scrollPane.setViewportView(tableLotes);
		
		JLabel lblLeilao = new JLabel("");
		lblLeilao.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblLeilao.setBounds(48, 11, 129, 29);
		add(lblLeilao);
		lblLeilao.setText("Leilao: " + leilao.getId());
		
		JLabel lblLotes = new JLabel("Clique em cima do lote desejado!");
		lblLotes.setBounds(48, 156, 213, 14);
		add(lblLotes);
		
		JLabel lblNatureza = new JLabel("");
		lblNatureza.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNatureza.setBounds(48, 47, 129, 22);
		lblNatureza.setText(leilao.getNatureza());
		add(lblNatureza);
		

		JLabel lblFormaLance = new JLabel("");
		lblFormaLance.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblFormaLance.setBounds(48, 70, 129, 22);
		lblFormaLance.setText(leilao.getFormaLance());
		add(lblFormaLance);
		
		
		JLabel lblStatus = new JLabel("");
		lblStatus.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblStatus.setBounds(48, 92, 129, 22);
		add(lblStatus);
		LocalDateTime hoje = LocalDateTime.now();
		if (hoje.isAfter(leilao.getDataHoraInicio()) && hoje.isBefore(leilao.getDataHoraFim()))
			lblStatus.setText("DISPONIVEL");
		else
			lblStatus.setText("INDISPONVEL");
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.trocaPanel(new Home(frame));
			}
		});
		btnVoltar.setBounds(459, 411, 89, 23);
		add(btnVoltar);
		
		tableLotes.getTableHeader().setVisible(true);
		String bens = "";
		for (Lote l : leilao.getLotes()) {
			bens = "";
			if (l.getBens() != null) {
			for (Bem bem : l.getBens()) {
				bens += bem.getDescricaoBreve() + " - ";
			}
			bens = bens.substring (0, bens.length() - 2);
			Object[] objs = {"R$ "+String.valueOf(l.getValor()), bens};
			tableModel.addRow(objs);
			}
        }
		
		tableLotes.addMouseListener(new java.awt.event.MouseAdapter() {
		    @Override
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		        int row = tableLotes.rowAtPoint(evt.getPoint());
		        int col = tableLotes.columnAtPoint(evt.getPoint());
		        if (row >= 0 && col >= 0) {
		        	
		        	Lote lote = leilao.getLotes().get(row);
		        	
		        	frame.trocaPanel(new DetalheLote(frame, lote, leilao, lblStatus.getText()));
		        }
		    }
		});
	}
}
