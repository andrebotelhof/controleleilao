package br.com.controleleilao.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.com.controleleilao.negocio.Bem;
import br.com.controleleilao.negocio.BemException;
import br.com.controleleilao.negocio.BemFachada;
import br.com.controleleilao.negocio.UsuarioException;
import br.com.controleleilao.negocio.UsuarioFachada;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import java.awt.Font;

public class CadastroBem extends JPanel {

	
	private JTextField textDescricaoBreve;
	private JTextField textDescricaoCompleta;
	private BemFachada bemFachada;
	private JTextField textCategoria;
	
	public CadastroBem(ControleTelaJFrame frame) {
		super();
		setBounds(100, 100, 614, 487);
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setLayout(null);
		
		JLabel lblNome = new JLabel("Descricao Breve");
		lblNome.setBounds(38, 87, 114, 16);
		this.add(lblNome);
		
		JLabel lblNewLabel = new JLabel("Descricao Completa");
		lblNewLabel.setBounds(38, 137, 137, 16);
		this.add(lblNewLabel);
		
		textDescricaoBreve = new JTextField();
		textDescricaoBreve.setBounds(38, 102, 544, 26);
		this.add(textDescricaoBreve);
		textDescricaoBreve.setColumns(10);
		
		textDescricaoCompleta = new JTextField();
		textDescricaoCompleta.setBounds(38, 159, 544, 262);
		this.add(textDescricaoCompleta);
		textDescricaoCompleta.setColumns(10);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				bemFachada = new BemFachada();
				String descricaoBreve = textDescricaoBreve.getText();
				String descricaoCompleta = textDescricaoCompleta.getText();
				String categoria = textCategoria.getText();
				try {
					bemFachada.validaBem(descricaoBreve, descricaoCompleta, categoria);
					frame.adicionaBem(new Bem(descricaoBreve, descricaoCompleta, categoria));
					textDescricaoBreve.setText("");
					textDescricaoCompleta.setText("");
					textCategoria.setText("");
					frame.trocaPanel(new CadastroLote(frame));
				} catch (BemException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
		}
		});
		btnCadastrar.setBounds(38, 432, 117, 29);
		this.add(btnCadastrar);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.trocaPanel(new CadastroLote(frame));
			}
		});
		btnVoltar.setBounds(487, 432, 117, 29);
		this.add(btnVoltar);
		
		JLabel lblCadastroDeBem = new JLabel("Cadastro de Bem");
		lblCadastroDeBem.setFont(new Font("Lucida Grande", Font.BOLD, 16));
		lblCadastroDeBem.setBounds(248, 11, 137, 16);
		this.add(lblCadastroDeBem);
		
		textCategoria = new JTextField();
		textCategoria.setColumns(10);
		textCategoria.setBounds(38, 49, 544, 26);
		this.add(textCategoria);
		
		JLabel lblCategoria = new JLabel("Categoria");
		lblCategoria.setBounds(38, 34, 114, 16);
		this.add(lblCategoria);
	}
}
