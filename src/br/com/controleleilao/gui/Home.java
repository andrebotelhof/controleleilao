package br.com.controleleilao.gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import br.com.controleleilao.negocio.Bem;
import br.com.controleleilao.negocio.BemException;
import br.com.controleleilao.negocio.Leilao;
import br.com.controleleilao.negocio.LeilaoException;
import br.com.controleleilao.negocio.LeilaoFachada;
import br.com.controleleilao.negocio.Lote;
import br.com.controleleilao.negocio.LoteException;
import br.com.controleleilao.negocio.Usuario;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class Home extends JPanel {
	private JTable tableLeiloes;
    private ArrayList<Leilao> listLeilao;
	/**
	 * Create the panel.
	 */
	public Home(ControleTelaJFrame frame) {
		super();
		setLayout(null);
		
		JLabel lblNameUser = new JLabel("");
		lblNameUser.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNameUser.setBounds(10, 11, 414, 55);
		add(lblNameUser);
		lblNameUser.setText(frame.getUserLogado().getNome());
		
		JLabel lblLeiloes = new JLabel("Leiloes:");
		lblLeiloes.setBounds(6, 144, 61, 16);
		add(lblLeiloes);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 180, 602, 263);
		add(scrollPane);
		
		Object[] colunas = {"Id Leilao", "Natureza", "Forma lance", "Data inicia", "Data fim", "Status"};
		DefaultTableModel tableModel = new DefaultTableModel(colunas, 0);
		
		tableLeiloes = new JTable(tableModel);
		scrollPane.setViewportView(tableLeiloes);
		
		
		LeilaoFachada fachada = new LeilaoFachada();
		try {
			 listLeilao = fachada.listLeiloes();
			
			for (Leilao b : listLeilao) {
				String status = "INDISPONIVEL";
				LocalDateTime hoje = LocalDateTime.now();
				if (hoje.isAfter(b.getDataHoraInicio()) && hoje.isBefore(b.getDataHoraFim()))
					status = "DISPONIVEL";
				Object[] objs = {b.getId(), b.getNatureza(), b.getFormaLance(), b.getDataHoraInicio(),b.getDataHoraFim() , status};
				tableModel.addRow(objs);
	        }
			
		} catch (LeilaoException | LoteException | BemException e1) {
			e1.printStackTrace();
		}
		
		tableLeiloes.addMouseListener(new java.awt.event.MouseAdapter() {
		    @Override
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		        int row = tableLeiloes.rowAtPoint(evt.getPoint());
		        int col = tableLeiloes.columnAtPoint(evt.getPoint());
		        if (row >= 0 && col >= 0) {
		        	
		        	Leilao leilao = listLeilao.get(row);
		        	
		        	frame.trocaPanel(new DetalheLeilao(frame, leilao));
		        }
		    }
		});
		
		JButton btnNovoLeilo = new JButton("Novo Leilao");
		btnNovoLeilo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.trocaPanel(new CadastroLeilao(frame));
			}
		});
		btnNovoLeilo.setBounds(491, 139, 117, 29);
		add(btnNovoLeilo);
		
		

	}
}
