package br.com.controleleilao.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.com.controleleilao.negocio.Usuario;
import br.com.controleleilao.negocio.UsuarioException;
import br.com.controleleilao.negocio.UsuarioFachada;

import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class Login extends JPanel {

	private JTextField textUsuario;
	private JPasswordField passwordField;

	public Login(ControleTelaJFrame frame) {
		super();
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		//setContentPane(this);
		this.setLayout(null);
		JLabel lblUsurio = new JLabel("Usuario (Email)");
		lblUsurio.setBounds(130, 50, 136, 16);
		this.add(lblUsurio);

		JLabel lblSenha = new JLabel("Senha (CPF / CNPJ)");
		lblSenha.setBounds(130, 111, 148, 16);
		this.add(lblSenha);
		
		textUsuario = new JTextField();
		textUsuario.setBounds(130, 73, 390, 26);
		this.add(textUsuario);
		textUsuario.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(130, 139, 390, 26);
		this.add(passwordField);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UsuarioFachada usuarioFachada = new UsuarioFachada();
				try {
					Usuario user = usuarioFachada.loginUsuario(textUsuario.getText(), passwordField.getText());
					textUsuario.setText("");
					passwordField.setText("");
					frame.setUserLogado(user);
					frame.trocaPanel(new Home(frame));
				} catch (UsuarioException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
				
			}
		});
		btnEntrar.setBounds(130, 188, 117, 29);
		this.add(btnEntrar);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.trocaPanel(new CadastroUsuario(frame));
			}
		});
		btnCadastrar.setBounds(403, 188, 117, 29);
		this.add(btnCadastrar);
		
		JLabel lblLogin = new JLabel("Login");
		lblLogin.setFont(new Font("Lucida Grande", Font.BOLD, 16));
		lblLogin.setBounds(281, 6, 46, 16);
		this.add(lblLogin);
		
	}
}
