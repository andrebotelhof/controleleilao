package br.com.controleleilao.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.text.Utilities;

import br.com.controleleilao.negocio.Bem;
import br.com.controleleilao.negocio.BemException;
import br.com.controleleilao.negocio.BemFachada;
import br.com.controleleilao.negocio.Lote;
import br.com.controleleilao.negocio.LoteException;
import br.com.controleleilao.negocio.LoteFachada;
import br.com.controleleilao.negocio.UsuarioException;
import br.com.controleleilao.negocio.UsuarioFachada;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.JList;
import javax.swing.JTable;
import java.awt.ScrollPane;
import javax.swing.JScrollBar;

public class CadastroLote extends JPanel {

	private JTextField textValor;
	private LoteFachada loteFachada = new LoteFachada();
	private JTable tableBens;
	
	public CadastroLote(ControleTelaJFrame frame) {
		super();
		setBounds(100, 100, 614, 487);
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setLayout(null);
		
		JLabel lblValor = new JLabel("Valor");
		lblValor.setBounds(38, 45, 61, 16);
		this.add(lblValor);
		
		textValor = new JTextField();
		textValor.setBounds(38, 60, 159, 26);
		this.add(textValor);
		textValor.setColumns(10);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Lote lote = new Lote();
				try {
					loteFachada.validaLote(textValor.getText(), frame.retornaBens());
					lote.setValor(Float.valueOf(textValor.getText()));
					lote.setBens(frame.retornaBens());
					frame.setBens(new ArrayList<Bem>());
					frame.adicionaLote(lote);
					frame.trocaPanel(new CadastroLeilao(frame));
				} catch (LoteException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
		}
		});
		btnCadastrar.setBounds(26, 421, 117, 29);
		this.add(btnCadastrar);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.trocaPanel(new CadastroLeilao(frame));
			}
		});
		btnVoltar.setBounds(432, 421, 117, 29);
		this.add(btnVoltar);
		
		JLabel lblCadastroDeUsurio = new JLabel("Cadastro de Lote");
		lblCadastroDeUsurio.setFont(new Font("Lucida Grande", Font.BOLD, 16));
		lblCadastroDeUsurio.setBounds(235, 11, 141, 16);
		this.add(lblCadastroDeUsurio);
		
		JButton btnAdicionarBem = new JButton("Adicionar Bem");
		btnAdicionarBem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.trocaPanel(new CadastroBem(frame));
			}
		});
		btnAdicionarBem.setBounds(35, 98, 162, 29);
		this.add(btnAdicionarBem);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(38, 149, 548, 260);
		add(scrollPane);
		Object[] colunas = {"Descricao Breve", "Descricao Completa", "Categoria"};
		DefaultTableModel tableModel = new DefaultTableModel(colunas, 0);
		tableBens = new JTable(tableModel);
		scrollPane.setViewportView(tableBens);
		Lote lote = new Lote();
		frame.adicionaLote(lote);
		for (Bem b : frame.retornaBens()) {
			Object[] objs = {b.getDescricaoBreve(), b.getDescricaoCompleta(), b.getCategoria()};
			tableModel.addRow(objs);
        }
	}
}
