package br.com.controleleilao.gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import br.com.controleleilao.negocio.Bem;
import br.com.controleleilao.negocio.BemException;
import br.com.controleleilao.negocio.Lance;
import br.com.controleleilao.negocio.LanceException;
import br.com.controleleilao.negocio.LanceFachada;
import br.com.controleleilao.negocio.Leilao;
import br.com.controleleilao.negocio.LeilaoException;
import br.com.controleleilao.negocio.LeilaoFachada;
import br.com.controleleilao.negocio.Lote;
import br.com.controleleilao.negocio.LoteException;
import br.com.controleleilao.negocio.Usuario;
import br.com.controleleilao.negocio.UsuarioException;
import br.com.controleleilao.negocio.UsuarioFachada;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JComboBox;

public class DetalheLote extends JPanel {

	private LeilaoFachada leilaoFachada = new LeilaoFachada();
	private JTextField textMinutoInicio;
	private JTextField textHoraInicio;
	private JTextField textDiaInicio;
	private JTextField textMesInicio;
	private JTextField textAnoInicio;
	private JTextField textAnoFim;
	private JTextField textMesFim;
	private JTextField textDiaFim;
	private JTextField textHoraFim;
	private JTextField textMinutoFim;
	private JTable tableBens;
	
	public DetalheLote(ControleTelaJFrame frame, Lote lote, Leilao leilao, String status) {
		super();
		setBounds(100, 100, 614, 487);
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setLayout(null);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.trocaPanel(new DetalheLeilao(frame, leilao));
			}
		});
		btnVoltar.setBounds(497, 379, 89, 23);
		add(btnVoltar);
		
		JLabel lblLeilao = new JLabel("");
		lblLeilao.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblLeilao.setBounds(48, 11, 129, 22);
		add(lblLeilao);
		lblLeilao.setText("Lote: " + lote.getId());
		
		JLabel lblNatureza = new JLabel("");
		lblNatureza.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNatureza.setBounds(48, 44, 103, 14);
		lblNatureza.setText(leilao.getNatureza());
		add(lblNatureza);
		
		JLabel lblFormaLance = new JLabel("");
		lblFormaLance.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblFormaLance.setBounds(48, 69, 103, 14);
		lblFormaLance.setText(leilao.getFormaLance());
		add(lblFormaLance);
		
		JLabel lblStatus = new JLabel("");
		lblStatus.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblStatus.setBounds(48, 94, 103, 14);
		lblStatus.setText(status);
		add(lblStatus);
		
		//tabela
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(38, 149, 548, 219);
		add(scrollPane);
		Object[] colunas = {"Descricao Breve", "Descricao Completa", "Categoria"};
		DefaultTableModel tableModel = new DefaultTableModel(colunas, 0);
		tableBens = new JTable(tableModel);
		scrollPane.setViewportView(tableBens);
		
		for (Bem b : lote.getBens()) {
			Object[] objs = {b.getDescricaoBreve(), b.getDescricaoCompleta(), b.getCategoria()};
			tableModel.addRow(objs);
        }
		
		LanceFachada lanceFachada = new LanceFachada();
		ArrayList<Lance> lanceList = new ArrayList<Lance>();
		ArrayList<Lance> lancesUsersLote = new ArrayList<Lance>();
		boolean verificador = false;
		try {
			lanceList = lanceFachada.listaLancesPorLote(lote.getId());
		} catch (LanceException e2) {
			JOptionPane.showMessageDialog(null, e2.getMessage());
		}
		for (Lance lance : lanceList) {
			if (lance.getIdLote()==lote.getId() && lance.getComprador().getId()==frame.getUserLogado().getId()) {
				verificador=true;
				lancesUsersLote.add(lance);
			}
		}
		
		
		if ((leilao.getFormaLance().equals("ABERTO") && status.equals("DISPONIVEL")) || (status.equals("INDISPONIVEL"))) {
		JButton btnVerLances = new JButton("Ver lances");
		btnVerLances.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String lances = "";
				try {
					for (Lance lance : lanceFachada.listaLancesPorLote(lote.getId()))
						lances += lance.toString() + " \n";
				} catch (LanceException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
				if (lances.equals("")) 
					JOptionPane.showMessageDialog(null, "Nao posui lances cadastrados!");
				else
				JOptionPane.showMessageDialog(null, lances);
			}
		});
		btnVerLances.setBounds(473, 115, 113, 23);
			add(btnVerLances);
		}
		
		if (status.equals("DISPONIVEL") && verificador==false) {
		JButton btnFazerLance = new JButton("Fazer lance");
		btnFazerLance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LocalDateTime agora = LocalDateTime.now();
				String valor = JOptionPane.showInputDialog("Digite o valor do lance: ");
				try {
					lanceFachada.cadastraLance(agora, valor, frame.getUserLogado(), lote.getId());
					JOptionPane.showMessageDialog(null, "Lance cadastrado com sucesso!");
					frame.trocaPanel(new DetalheLote(frame, lote, leilao, status));
				} catch (LanceException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
			}
		});
		btnFazerLance.setBounds(38, 119, 113, 23);
		add(btnFazerLance);
		}
		
		else if (status.equals("DISPONIVEL") && verificador==true) {
		JButton btnFazerLance = new JButton("Cancelar Lance");
		btnFazerLance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					for (Lance lance : lancesUsersLote) {
						lanceFachada.cancelaLance(lance.getIdLance());
					}
					JOptionPane.showMessageDialog(null, "Lances cancelado com sucesso!");
					frame.trocaPanel(new DetalheLote(frame, lote, leilao, status));
				} catch (LanceException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
			}
		});
		btnFazerLance.setBounds(38, 119, 113, 23);
		add(btnFazerLance);
		}
	}
}
