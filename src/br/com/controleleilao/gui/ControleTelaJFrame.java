package br.com.controleleilao.gui;

import java.awt.Container;
import java.awt.EventQueue;
import java.util.ArrayList;

import br.com.controleleilao.negocio.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import br.com.controleleilao.negocio.Leilao;
	public class ControleTelaJFrame extends JFrame {
		private Usuario userLogado = new Usuario();
		private ArrayList<Bem> bens = new ArrayList<Bem>();
		private ArrayList<Lote> lotes = new ArrayList<Lote>();
		private Leilao leilao = new Leilao();
		
		public ControleTelaJFrame() { 
			setTitle("Controle de Leiloes");
			setResizable(false);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 614, 487);
			setContentPane(new Login(this));
			
		}
		
		public void trocaPanel (Container panel) {
			setContentPane(panel);
			repaint();
			revalidate();
		}
		
		public void adicionaBem(Bem bem) {
			bens.add(bem);
		}
		
		public void setBens(ArrayList<Bem> bens) {
			this.bens = bens;
		}
		
		public void setLotes(ArrayList<Lote> lotes) {
			this.lotes = lotes;
		}
		
		public ArrayList<Bem> retornaBens() {
			return bens;
		}
		
		public void adicionaLote(Lote lote) {
			lotes.add(lote);
		}
		
		public ArrayList<Lote> retornaLotes() {
			return lotes;
		}

		public Usuario getUserLogado() {
			return userLogado;
		}

		public void setUserLogado(Usuario userLogado) {
			this.userLogado = userLogado;
		}

		public Leilao getLeilao() {
			return leilao;
		}

		public void setLeilao(Leilao leilao) {
			this.leilao = leilao;
		}
		
		
	}
