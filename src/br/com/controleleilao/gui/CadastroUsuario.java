package br.com.controleleilao.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.com.controleleilao.negocio.Usuario;
import br.com.controleleilao.negocio.UsuarioException;
import br.com.controleleilao.negocio.UsuarioFachada;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import java.awt.Font;

public class CadastroUsuario extends JPanel {

	private JTextField textNome;
	private JTextField textCpfCnpj;
	private JTextField textEmail;
	private UsuarioFachada usuarioFachada;
	
	public CadastroUsuario(ControleTelaJFrame frame) {
		super();
		setLayout(null);
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(52, 45, 37, 16);
		this.add(lblNome);
		
		JLabel lblNewLabel = new JLabel("CPF/CNPJ");
		lblNewLabel.setBounds(52, 107, 60, 16);
		this.add(lblNewLabel);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(52, 174, 34, 16);
		this.add(lblEmail);
		
		textNome = new JTextField();
		textNome.setBounds(52, 73, 520, 26);
		this.add(textNome);
		textNome.setColumns(10);
		
		textCpfCnpj = new JTextField();
		textCpfCnpj.setBounds(52, 136, 520, 26);
		this.add(textCpfCnpj);
		textCpfCnpj.setColumns(10);
		
		textEmail = new JTextField();
		textEmail.setBounds(52, 200, 520, 26);
		this.add(textEmail);
		textEmail.setColumns(10);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					usuarioFachada = new UsuarioFachada();
					String nome = textNome.getText();
					String cpfCnpj = textCpfCnpj.getText();
					String email = textEmail.getText();
					usuarioFachada.cadastraUsuario(nome, cpfCnpj, email);
					JOptionPane.showMessageDialog(null, "Cadastro concluido com sucesso.");
					frame.trocaPanel(new Login(frame));
				} catch (UsuarioException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
		}
		});
		btnCadastrar.setBounds(157, 273, 104, 29);
		this.add(btnCadastrar);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.trocaPanel(new Login(frame));
			}
		});
		btnVoltar.setBounds(337, 273, 81, 29);
		this.add(btnVoltar);
		
		JLabel lblCadastroDeUsurio = new JLabel("Cadastro de Usuario");
		lblCadastroDeUsurio.setFont(new Font("Lucida Grande", Font.BOLD, 16));
		lblCadastroDeUsurio.setBounds(239, 6, 165, 20);
		this.add(lblCadastroDeUsurio);
	}
}
