package br.com.controleleilao.gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import br.com.controleleilao.negocio.Bem;
import br.com.controleleilao.negocio.BemException;
import br.com.controleleilao.negocio.Leilao;
import br.com.controleleilao.negocio.LeilaoException;
import br.com.controleleilao.negocio.LeilaoFachada;
import br.com.controleleilao.negocio.Lote;
import br.com.controleleilao.negocio.LoteException;
import br.com.controleleilao.negocio.Usuario;
import br.com.controleleilao.negocio.UsuarioException;
import br.com.controleleilao.negocio.UsuarioFachada;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JComboBox;

public class CadastroLeilao extends JPanel {

	private LeilaoFachada leilaoFachada = new LeilaoFachada();
	private JTextField textMinutoInicio;
	private JTextField textHoraInicio;
	private JTextField textDiaInicio;
	private JTextField textMesInicio;
	private JTextField textAnoInicio;
	private JTextField textAnoFim;
	private JTextField textMesFim;
	private JTextField textDiaFim;
	private JTextField textHoraFim;
	private JTextField textMinutoFim;
	private JTable tableLotes;
	
	public CadastroLeilao(ControleTelaJFrame frame) {
		super();
		setBounds(100, 100, 614, 487);
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setLayout(null);
		
		JLabel lblNatureza = new JLabel("Natureza");
		lblNatureza.setBounds(38, 45, 61, 16);
		this.add(lblNatureza);
		
		JComboBox comboBoxNatureza = new JComboBox();
		comboBoxNatureza.addItem("OFERTA");
		comboBoxNatureza.addItem("DEMANDA");
		comboBoxNatureza.setBounds(38, 62, 235, 27);
		this.add(comboBoxNatureza);
		
		JComboBox comboBoxFormaLance = new JComboBox();
		comboBoxFormaLance.addItem("ABERTO");
		comboBoxFormaLance.addItem("FECHADO");
		comboBoxFormaLance.setBounds(335, 62, 235, 27);
		this.add(comboBoxFormaLance);
		
		JLabel lblFormaLance = new JLabel("Forma de Lance");
		lblFormaLance.setBounds(335, 45, 100, 16);
		this.add(lblFormaLance);
		
		JLabel lblEmail = new JLabel("Inicio do Leilao");
		lblEmail.setBounds(38, 101, 235, 16);
		this.add(lblEmail);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					leilaoFachada.validaLeilao(comboBoxNatureza.getSelectedItem().toString(), comboBoxFormaLance.getSelectedItem().toString(), textAnoInicio.getText(), 
							textMesInicio.getText(), textDiaInicio.getText(), textHoraInicio.getText(), 
							textMinutoInicio.getText(), textAnoFim.getText(), textMesFim.getText(), textDiaFim.getText(), 
							textHoraFim.getText(), textMinutoFim.getText(), frame.getUserLogado());
					int anoFloat = Integer.valueOf(textAnoInicio.getText()).intValue();
					int mesFloat = Integer.valueOf(textMesInicio.getText()).intValue();
					int diaFloat = Integer.valueOf(textDiaInicio.getText()).intValue();
					int horaFloat = Integer.valueOf(textHoraInicio.getText()).intValue();
					int minutoFloat = Integer.valueOf(textMinutoFim.getText()).intValue();
					int anoFimFloat = Integer.valueOf(textAnoFim.getText()).intValue();
					int mesFimFloat = Integer.valueOf(textMesFim.getText()).intValue();
					int diaFimFloat = Integer.valueOf(textDiaFim.getText()).intValue();
					int horaFimFloat = Integer.valueOf(textHoraFim.getText()).intValue();
					int minutoFimFloat = Integer.valueOf(textMinutoFim.getText()).intValue();
					LocalDateTime dataInicio = LocalDateTime.of(anoFloat, mesFloat, diaFloat, horaFloat, minutoFloat);
					LocalDateTime dataFim = LocalDateTime.of(anoFimFloat, mesFimFloat, diaFimFloat, horaFimFloat, minutoFimFloat);
					//Usuario user = new Usuario();
					//user.setId(10);
					Leilao leilao = new Leilao(comboBoxNatureza.getSelectedItem().toString(), comboBoxFormaLance.getSelectedItem().toString(), 
							dataInicio, dataFim, frame.getUserLogado());
					leilao.setLotes(frame.retornaLotes());
					leilaoFachada.cadastraLeilao(leilao);
					frame.setLotes(new ArrayList<Lote>());
					JOptionPane.showMessageDialog(null, "Leilao cadastrado com sucesso!");
					frame.trocaPanel(new Home(frame));
				} catch (LeilaoException | LoteException | BemException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
				}
		}
		});
		btnCadastrar.setBounds(33, 434, 117, 29);
		this.add(btnCadastrar);
		
		JButton btnVoltar = new JButton("Cancelar");
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.trocaPanel(new Home(frame));
			}
		});
		btnVoltar.setBounds(453, 434, 117, 29);
		this.add(btnVoltar);
		
		JLabel lblCadastroDeUsurio = new JLabel("Cadastro de Leilao");
		lblCadastroDeUsurio.setFont(new Font("Lucida Grande", Font.BOLD, 16));
		lblCadastroDeUsurio.setBounds(233, 6, 149, 16);
		this.add(lblCadastroDeUsurio);
		
		JLabel lblFimDoLeilo = new JLabel("Fim do Leilao");
		lblFimDoLeilo.setBounds(335, 101, 235, 16);
		this.add(lblFimDoLeilo);
		
		JLabel lblAnoInicio = new JLabel("Ano");
		lblAnoInicio.setBounds(38, 121, 61, 16);
		this.add(lblAnoInicio);
		
		JLabel lblMesInicio = new JLabel("Mes");
		lblMesInicio.setBounds(38, 149, 61, 16);
		this.add(lblMesInicio);
		
		JLabel lblDiaInicio = new JLabel("Dia");
		lblDiaInicio.setBounds(38, 180, 61, 16);
		this.add(lblDiaInicio);
		
		JLabel lblHoraInicio = new JLabel("Hora");
		lblHoraInicio.setBounds(38, 208, 61, 16);
		this.add(lblHoraInicio);
		
		JLabel lblMinutoInicio = new JLabel("Minuto");
		lblMinutoInicio.setBounds(38, 236, 61, 16);
		this.add(lblMinutoInicio);
		
		textMinutoInicio = new JTextField();
		textMinutoInicio.setBounds(96, 231, 130, 26);
		this.add(textMinutoInicio);
		textMinutoInicio.setColumns(10);
		
		textHoraInicio = new JTextField();
		textHoraInicio.setColumns(10);
		textHoraInicio.setBounds(96, 203, 130, 26);
		this.add(textHoraInicio);
		
		textDiaInicio = new JTextField();
		textDiaInicio.setColumns(10);
		textDiaInicio.setBounds(96, 175, 130, 26);
		this.add(textDiaInicio);
		
		textMesInicio = new JTextField();
		textMesInicio.setColumns(10);
		textMesInicio.setBounds(96, 144, 130, 26);
		this.add(textMesInicio);
		
		textAnoInicio = new JTextField();
		textAnoInicio.setColumns(10);
		textAnoInicio.setBounds(96, 116, 130, 26);
		this.add(textAnoInicio);
		
		textAnoFim = new JTextField();
		textAnoFim.setColumns(10);
		textAnoFim.setBounds(393, 116, 130, 26);
		this.add(textAnoFim);
		
		JLabel lblAnoFim = new JLabel("Ano");
		lblAnoFim.setBounds(335, 121, 61, 16);
		this.add(lblAnoFim);
		
		JLabel lblMesFim = new JLabel("Mes");
		lblMesFim.setBounds(335, 149, 61, 16);
		this.add(lblMesFim);
		
		textMesFim = new JTextField();
		textMesFim.setColumns(10);
		textMesFim.setBounds(393, 144, 130, 26);
		this.add(textMesFim);
		
		textDiaFim = new JTextField();
		textDiaFim.setColumns(10);
		textDiaFim.setBounds(393, 175, 130, 26);
		this.add(textDiaFim);
		
		textHoraFim = new JTextField();
		textHoraFim.setColumns(10);
		textHoraFim.setBounds(393, 203, 130, 26);
		this.add(textHoraFim);
		
		textMinutoFim = new JTextField();
		textMinutoFim.setColumns(10);
		textMinutoFim.setBounds(393, 231, 130, 26);
		this.add(textMinutoFim);
		
		JLabel lblMinutoFim = new JLabel("Minuto");
		lblMinutoFim.setBounds(335, 236, 61, 16);
		this.add(lblMinutoFim);
		
		JLabel lblHoraFim = new JLabel("Hora");
		lblHoraFim.setBounds(335, 208, 61, 16);
		this.add(lblHoraFim);
		
		JLabel lblDiaFim = new JLabel("Dia");
		lblDiaFim.setBounds(335, 180, 61, 16);
		this.add(lblDiaFim);
		
		JButton btnAdicionarLote = new JButton("Adicionar Lote");
		btnAdicionarLote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.trocaPanel(new CadastroLote(frame));
			}
		});
		btnAdicionarLote.setBounds(33, 269, 117, 29);
		this.add(btnAdicionarLote);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(48, 301, 500, 123);
		add(scrollPane);
		
		
		Object[] colunas = {"Valor", "Bens"};
		DefaultTableModel tableModel = new DefaultTableModel(colunas, 0);
		tableModel.setColumnIdentifiers(colunas);
		tableLotes = new JTable(tableModel);
		scrollPane.setViewportView(tableLotes);
		tableLotes.getTableHeader().setVisible(true);
		String bens = "";
		for (Lote l : frame.retornaLotes()) {
			bens = "";
			if (l.getBens() != null) {
			for (Bem bem : l.getBens()) {
				bens += bem.getDescricaoBreve() + " - ";
			}
			bens = bens.substring (0, bens.length() - 2);
			Object[] objs = {"R$ "+String.valueOf(l.getValor()), bens};
			tableModel.addRow(objs);
			}
        }
	}
}
