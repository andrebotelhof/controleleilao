package br.com.controleleilao.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import br.com.controleleilao.negocio.Bem;
import br.com.controleleilao.negocio.Lote;

public class BemDAO {
	
	public BemDAO() {
		
	}
	
	public int cadastraBem(Bem bem, int idLote) throws BemDAOException {
		int id = 0;
		try {
			Connection conexao = new ConexaoUtil().getConnection();

			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO bem (descricao_breve, descricao_completa, categoria) ");
			sql.append("VALUES (?, ?, ?)");

			PreparedStatement statement = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, bem.getDescricaoBreve());
			statement.setString(2, bem.getDescricaoCompleta());
			statement.setString(3, bem.getCategoria());
			statement.executeUpdate();

			ResultSet resultset = statement.getGeneratedKeys();
			if (resultset.first()) {
				id = resultset.getInt(1);
			}
			bem.setId(id);
			cadastraBemLote(bem.getId(), idLote);
			conexao.close();
		} catch (SQLException e) {
			throw new BemDAOException("Erro para cadastrar o bem!", e);
		}
		return id;
	}
	
	private void cadastraBemLote(int idBem, int idLote) throws BemDAOException {
		try {
			Connection conexao = new ConexaoUtil().getConnection();

			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO lote_bem (id_bem, id_lote) ");
			sql.append("VALUES (?, ?)");

			PreparedStatement statement = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, idBem);
			statement.setInt(2, idLote);
			statement.executeUpdate();
			conexao.close();
		} catch (SQLException e) {
			throw new BemDAOException("Erro para cadastrar o bem em relacao a um lote!", e);
		}
	}
	
	public ArrayList<Bem> listaBemPorLote(int idLote) throws BemDAOException {
		ArrayList<Bem> bens = new ArrayList<>();
		try {
			Connection conexao = new ConexaoUtil().getConnection();
		
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT id_bem, descricao_breve, descricao_completa, categoria ");
			sql.append("FROM bem ");
			sql.append("WHERE id_bem IN ( ");
			sql.append(" SELECT id_bem ");
			sql.append(" FROM lote_bem ");
			sql.append(" WHERE id_lote = ?) ");
			
			PreparedStatement statement = conexao.prepareStatement(sql.toString());
			statement.setInt(1, idLote);

			ResultSet resultset = statement.executeQuery();

			while (resultset.next()) {
				Bem bem = new Bem();
				bem.setId(resultset.getInt("id_bem"));
				bem.setDescricaoBreve(resultset.getString("descricao_breve"));
				bem.setDescricaoCompleta(resultset.getString("descricao_completa"));
				bem.setCategoria(resultset.getString("categoria"));
				bens.add(bem);
			}
			conexao.close();
		} catch (SQLException e) {
			throw new BemDAOException("Erro para cadastrar o leilao!", e);
		}	
		return bens;
	}
		
}

