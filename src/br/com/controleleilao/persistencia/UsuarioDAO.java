package br.com.controleleilao.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import br.com.controleleilao.negocio.Leilao;
import br.com.controleleilao.negocio.Usuario;

public class UsuarioDAO {
	
	public UsuarioDAO() {
		
	}
	
	public int cadastraUsuario(Usuario user) throws UsuarioDAOException {
		int id = 0;
		try {
			Connection conexao = new ConexaoUtil().getConnection();

			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO usuario (nome, cpf_cnpj, email) ");
			sql.append("VALUES (?, ?, ?)");

			PreparedStatement statement = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, user.getNome());
			statement.setString(2, user.getCpfCnpj());
			statement.setString(3, user.getEmail());
			statement.executeUpdate();

			ResultSet resultset = statement.getGeneratedKeys();
			if (resultset.first()) {
				id = resultset.getInt(1);

			}
		} catch (SQLException e) {
			throw new UsuarioDAOException("Erro para cadastrar o usuario!", e);
		}
		return id;
	}
	//SELECT * FROM usuario WHERE email = 'andrebotelho@live.com' && cpf_cnpj = '123456789'
	public Usuario loginUsuario(String email, String cpfCnpj) throws UsuarioDAOException {
		Usuario usuario = new Usuario();
		try {
			Connection conexao = new ConexaoUtil().getConnection();
		
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT id_usuario, nome, cpf_cnpj, email ");
			sql.append("FROM usuario ");
			sql.append("WHERE email = ? && cpf_cnpj = ?");
			
		
			PreparedStatement statement = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, email);
			statement.setString(2, cpfCnpj);
			//statement.executeUpdate();
			
			ResultSet resultset = statement.executeQuery();
			if (resultset.next()) {
				usuario.setId(resultset.getInt("id_usuario"));
				usuario.setNome(resultset.getString("nome"));
				usuario.setCpf_cnpj(resultset.getString("cpf_cnpj"));
				usuario.setEmail(resultset.getString("email"));
			}
			else {
				throw new UsuarioDAOException("Usuario nao cadastrado!");
			}
			conexao.close();
			return usuario;
		} catch (SQLException e) {
			throw new UsuarioDAOException("Erro para efetuar o login!", e);
		}
	}
}

