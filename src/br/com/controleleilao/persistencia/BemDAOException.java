package br.com.controleleilao.persistencia;

public class BemDAOException extends Exception{
	public BemDAOException() {
    }

    public BemDAOException(String msg) {
        super(msg);
    }

    public BemDAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
