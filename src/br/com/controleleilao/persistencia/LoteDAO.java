package br.com.controleleilao.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import br.com.controleleilao.negocio.Leilao;
import br.com.controleleilao.negocio.Lote;
import br.com.controleleilao.negocio.Usuario;

public class LoteDAO {
	
	public LoteDAO() {
		
	}
	
	public int cadastraLote(Lote lote, int idLeilao) throws LoteDAOException {
		int id = 0;
		try {
			Connection conexao = new ConexaoUtil().getConnection();

			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO lote (id_leilao, valor) ");
			sql.append("VALUES (?, ?)");

			PreparedStatement statement = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, idLeilao);
			statement.setFloat(2, lote.getValor());
			statement.executeUpdate();

			ResultSet resultset = statement.getGeneratedKeys();
			if (resultset.first()) {
				id = resultset.getInt(1);

			}
			conexao.close();
		} catch (SQLException e) {
			throw new LoteDAOException("Erro para cadastrar o lote!", e);
		}
		return id;
	}
	
	public ArrayList<Lote> listaLotes(int idLeilao) throws LoteDAOException, BemDAOException {
		ArrayList<Lote> lotes = new ArrayList<>();
		BemDAO bemDAO = new BemDAO();
		try {
			Connection conexao = new ConexaoUtil().getConnection();
		
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT id_lote, id_leilao, valor ");
			sql.append("FROM lote ");
			sql.append("WHERE id_leilao = ? ");
			
			PreparedStatement statement = conexao.prepareStatement(sql.toString());
			statement.setInt(1, idLeilao);

			ResultSet resultset = statement.executeQuery();

			while (resultset.next()) {
				Lote lote = new Lote();
				lote.setId(resultset.getInt("id_lote"));
				//lote.setId(resultset.getInt("id_leilao"));
				lote.setValor(resultset.getFloat("valor"));
				lote.setBens(bemDAO.listaBemPorLote(lote.getId()));
				lotes.add(lote);
			}
			conexao.close();
		} catch (SQLException e) {
			throw new LoteDAOException("Erro para listar os lotes!", e);
		}	
		return lotes;
	}
		
}

