package br.com.controleleilao.persistencia;

public class LeilaoDAOException extends Exception{
	public LeilaoDAOException() {
    }

    public LeilaoDAOException(String msg) {
        super(msg);
    }

    public LeilaoDAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
