package br.com.controleleilao.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import br.com.controleleilao.negocio.Leilao;
import br.com.controleleilao.negocio.Usuario;

public class LeilaoDAO {
	
	public LeilaoDAO() {	
	}
	
	public int cadastraLeilao(Leilao leilao) throws LeilaoDAOException {
		int id = 0;
		try {
			Connection conexao = new ConexaoUtil().getConnection();

			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO leilao (natureza, forma_lance, data_hora_inicio, data_hora_fim, id_vendedor) ");
			sql.append("VALUES (?, ?, ?, ?, ?)");
			
			PreparedStatement statement = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, leilao.getNatureza());
			statement.setString(2, leilao.getFormaLance());
			statement.setTimestamp(3, java.sql.Timestamp.valueOf(leilao.getDataHoraInicio()));
			statement.setTimestamp(4, java.sql.Timestamp.valueOf(leilao.getDataHoraFim()));
			statement.setInt(5, leilao.getVendedor().getId());
			
			statement.executeUpdate();
			ResultSet resultset = statement.getGeneratedKeys();
			if (resultset.first()) {
				id = resultset.getInt(1);
			}
			conexao.close();
		} catch (SQLException e) {
			throw new LeilaoDAOException("Erro para cadastrar o leilao!", e);
		} 
		
		return id;
	}

	public ArrayList<Leilao> listaLeiloes() throws LeilaoDAOException, LoteDAOException, BemDAOException {
		ArrayList<Leilao> leiloes = new ArrayList<>();
		LoteDAO loteDAO = new LoteDAO();
		try {
			Connection conexao = new ConexaoUtil().getConnection();
		
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT id_leilao, natureza, forma_lance, data_hora_inicio, data_hora_fim, id_vendedor ");
			sql.append("FROM leilao ");
			
			PreparedStatement statement = conexao.prepareStatement(sql.toString());
			ResultSet resultset = statement.executeQuery();
			while (resultset.next()) {
				Leilao leilao = new Leilao();
				Usuario usuario = new Usuario();
				leilao.setId(resultset.getInt("id_leilao"));
				leilao.setNatureza(resultset.getString("natureza"));
				leilao.setFormaLance(resultset.getString("forma_lance"));
				leilao.setDataHoraInicio(resultset.getTimestamp("data_hora_inicio").toLocalDateTime());
				leilao.setDataHoraFim(resultset.getTimestamp("data_hora_fim").toLocalDateTime());
				usuario.setId(resultset.getInt("id_vendedor"));
				leilao.setVendedor(usuario);
				leilao.setLotes(loteDAO.listaLotes(leilao.getId()));
				leiloes.add(leilao);
			}
			conexao.close();
		} catch (SQLException e) {
			throw new LeilaoDAOException("Erro para listar leiloes", e);
		}	
		return leiloes;
	}
}

