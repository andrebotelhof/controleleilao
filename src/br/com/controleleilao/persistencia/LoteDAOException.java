package br.com.controleleilao.persistencia;

public class LoteDAOException extends Exception{
	public LoteDAOException() {
    }

    public LoteDAOException(String msg) {
        super(msg);
    }

    public LoteDAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
