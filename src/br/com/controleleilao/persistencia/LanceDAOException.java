package br.com.controleleilao.persistencia;

public class LanceDAOException extends Exception{
	public LanceDAOException() {
    }

    public LanceDAOException(String msg) {
        super(msg);
    }

    public LanceDAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
