package br.com.controleleilao.persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class ConexaoUtil {
	 public Connection getConnection() {
	        try {
	            return DriverManager.getConnection(
	          "jdbc:mysql://controleleilao.mysql.dbaas.com.br/controleleilao", "controleleilao", "leilao12345");
	        } catch (SQLException e) {
	            throw new RuntimeException(e);
	        }
	    }
}