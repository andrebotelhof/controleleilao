package br.com.controleleilao.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import br.com.controleleilao.negocio.Bem;
import br.com.controleleilao.negocio.Lance;
import br.com.controleleilao.negocio.Usuario;

public class LanceDAO {
	
	public LanceDAO() {
		
	}
	
	public int cadastraLance(Lance lance) throws LanceDAOException {
		int id = 0;
		try {
			Connection conexao = new ConexaoUtil().getConnection();

			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO lance (data_hora_lance, valor, id_usuario, id_lote) ");
			sql.append("VALUES (?, ?, ?, ?)");

			PreparedStatement statement = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			statement.setTimestamp(1, java.sql.Timestamp.valueOf(lance.getDataHoraLance()));
			statement.setFloat(2, lance.getValor());
			statement.setInt(3, lance.getComprador().getId());
			statement.setInt(4, lance.getIdLote());
			statement.executeUpdate();

			ResultSet resultset = statement.getGeneratedKeys();
			if (resultset.first()) {
				id = resultset.getInt(1);

			}
		} catch (SQLException e) {
			throw new LanceDAOException("Erro para cadastrar o lance!", e);
		}
		return id;
	}
	
	public boolean cancelaLance(int idLance) throws LanceDAOException {
		boolean ver = false;
		try {
			Connection conexao = new ConexaoUtil().getConnection();

			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM lance ");
			sql.append("WHERE id_lance = ?");

			PreparedStatement statement = conexao.prepareStatement(sql.toString());
			statement.setInt(1, idLance);
			statement.executeUpdate();

			ver =  statement.execute();
			conexao.close();
			return ver;
		} catch (SQLException e) {
			throw new LanceDAOException("Erro para cancelar o lance!", e);
		}
	}
	
	public ArrayList<Lance> listaLancesPorLote(int idLote) throws LanceDAOException {
		ArrayList<Lance> lances = new ArrayList<>();
		try {
			Connection conexao = new ConexaoUtil().getConnection();
		
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT id_lance, data_hora_lance, valor, id_usuario, id_lote ");
			sql.append("FROM lance ");
			sql.append("WHERE id_lote = ? ");
			
			PreparedStatement statement = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, idLote);

			ResultSet resultset = statement.executeQuery();

			while (resultset.next()) {
				Lance lance = new Lance();
				lance.setIdLance(resultset.getInt("id_lance"));
				lance.setDataHoraLance(resultset.getTimestamp("data_hora_lance").toLocalDateTime());
				lance.setValor(resultset.getFloat("valor"));
				Usuario user = new Usuario();
				user.setId(resultset.getInt("id_usuario"));
				lance.setComprador(user);
				lance.setIdLote(resultset.getInt("id_lote"));
				lances.add(lance);
			}
			conexao.close();
		} catch (SQLException e) {
			throw new LanceDAOException("Erro para listar os lances!", e);
		}	
		return lances;
	}
		
}

