package br.com.controleleilao.testes;

import static org.junit.Assert.*;

import org.junit.Test;

import br.com.controleleilao.negocio.ValidarUsuario;

public class ValidarUsuarioTeste {

	@Test
	public void testValidaNome() {
		assertEquals(ValidarUsuario.validaNome("Teste Teste2"), true);
		assertEquals(ValidarUsuario.validaNome("TesteTEste"), false);
		assertEquals(ValidarUsuario.validaNome(""), false);
	}

	@Test
	public void testValidaCpfCnPJ() {
		assertEquals(ValidarUsuario.validaCpfCnPJ("333.333.333-33"), false);
		assertEquals(ValidarUsuario.validaCpfCnPJ(""), false);
		assertEquals(ValidarUsuario.validaCpfCnPJ(" "), false);
	}

	@Test
	public void testValidaEmail() {
		assertEquals(ValidarUsuario.validaEmail("email@teste.com"), true);
		assertEquals(ValidarUsuario.validaEmail("email@testecom"), false);
		assertEquals(ValidarUsuario.validaEmail("emailteste.com"), false);
	}

}
