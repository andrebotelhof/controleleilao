package br.com.controleleilao.testes;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.Test;

import br.com.controleleilao.negocio.LanceException;
import br.com.controleleilao.negocio.Usuario;
import br.com.controleleilao.negocio.ValidaLance;

public class ValidaLanceTeste {

	@Test
	public void testValidaValor() throws LanceException {
		String valor = "10.00";
		assertEquals(ValidaLance.validaValor(valor), true);
		valor = "-10.00";
		assertEquals(ValidaLance.validaValor(valor), false);
		valor = "-10.00";
		assertEquals(ValidaLance.validaValor(valor), false);
	}

	@Test
	public void testValidaDataHora() {
		assertEquals(ValidaLance.validaDataHora(LocalDateTime.now()), true);
	}

	@Test
	public void testValidaUsuario() {
		Usuario user = new Usuario();
		user.setId(354);
		assertEquals(ValidaLance.validaUsuario(user), true);
		user.setId(0);
		assertEquals(ValidaLance.validaUsuario(user), false);
	}

	@Test
	public void testValidaLote() {
		assertEquals(ValidaLance.validaLote(10), true);
		assertEquals(ValidaLance.validaLote(00), false);
		assertEquals(ValidaLance.validaLote(-10), false);
	}

}
