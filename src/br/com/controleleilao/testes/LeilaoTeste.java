package br.com.controleleilao.testes;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.Test;

import br.com.controleleilao.negocio.Leilao;
import br.com.controleleilao.negocio.Lote;
import br.com.controleleilao.negocio.Usuario;

public class LeilaoTeste {
	Leilao leilao = new Leilao();
	
	@Test
	public void testLeilaoStringStringLocalDateTimeLocalDateTimeUsuario() {
		String natureza = "TesteNatureza";
		String formaLance = "TesteFormaLance";
		LocalDateTime agora = LocalDateTime.now();
		LocalDateTime dataHoraFim = LocalDateTime.now();
		Usuario user = new Usuario("nomeTeste", "333.3333.3333-3", "email@teste.com");
		
		Leilao leilaoTeste = new Leilao(natureza, formaLance, agora, dataHoraFim, user);
		assertEquals(leilaoTeste.getNatureza(), natureza);
		assertEquals(leilaoTeste.getFormaLance(), formaLance);
		assertEquals(leilaoTeste.getDataHoraInicio(), agora);
		assertEquals(leilaoTeste.getDataHoraFim(), dataHoraFim);
		assertEquals(leilaoTeste.getVendedor(), user);
	}

	@Test
	public void testLeilaoStringStringLocalDateTimeLocalDateTimeUsuarioArrayListOfLote() {
		String natureza = "TesteNatureza";
		String formaLance = "TesteFormaLance";
		LocalDateTime agora = LocalDateTime.now();
		LocalDateTime dataHoraFim = LocalDateTime.now();
		Usuario user = new Usuario("nomeTeste", "333.3333.3333-3", "email@teste.com");
		ArrayList<Lote> lotes = new ArrayList<Lote>();
		Lote lote1 = new Lote(100);
		lotes.add(lote1);
		
		Leilao leilaoTeste = new Leilao(natureza, formaLance, agora, dataHoraFim, user, lotes);
		
		assertEquals(leilaoTeste.getNatureza(), natureza);
		assertEquals(leilaoTeste.getFormaLance(), formaLance);
		assertEquals(leilaoTeste.getDataHoraInicio(), agora);
		assertEquals(leilaoTeste.getDataHoraFim(), dataHoraFim);
		assertEquals(leilaoTeste.getVendedor(), user);
		assertEquals(leilaoTeste.getLotes().get(0).getId(), lote1.getId());
	}

	@Test
	public void testSetId() {
		leilao.setId(132);
		assertEquals(leilao.getId(), 132);
	}

	@Test
	public void testSetNatureza() {
		String natureza = "natureza";
		leilao.setNatureza(natureza);
		assertEquals(leilao.getNatureza(), natureza);
	}

	@Test
	public void testSetFormaLance() {
		String formaLance = "formaLance";
		leilao.setFormaLance(formaLance);
		assertEquals(leilao.getFormaLance(), formaLance);
	}

	@Test
	public void testSetDataHoraInicio() {
		LocalDateTime agora = LocalDateTime.now();
		leilao.setDataHoraInicio(agora);
		assertEquals(leilao.getDataHoraInicio(), agora);
	}

	@Test
	public void testSetDataHoraFim() {
		LocalDateTime agora = LocalDateTime.now();
		leilao.setDataHoraFim(agora);
		assertEquals(leilao.getDataHoraFim(), agora);
	}

	@Test
	public void testSetVendedor() {
		Usuario user = new Usuario("nomeTeste", "333.3333.3333-3", "email@teste.com");
		leilao.setVendedor(user);
		assertEquals(leilao.getVendedor(), user);
	}

	@Test
	public void testSetLotes() {
		ArrayList<Lote> lotes = new ArrayList<Lote>();
		Lote lote1 = new Lote(100);
		Lote lote2 = new Lote(200);
		Lote lote3 = new Lote(300);
		lotes.add(lote1);
		lotes.add(lote2);
		lotes.add(lote3);
		
		leilao.setLotes(lotes);
		assertEquals(leilao.getLotes().get(0).getValor(), lote1.getValor(), 0.000);
		assertEquals(leilao.getLotes().get(1).getValor(), lote2.getValor(), 0.000);
		assertEquals(leilao.getLotes().get(2).getValor(), lote3.getValor(), 0.000);
	}

}
