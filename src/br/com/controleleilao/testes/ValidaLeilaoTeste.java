package br.com.controleleilao.testes;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.junit.Test;

import br.com.controleleilao.negocio.Lote;
import br.com.controleleilao.negocio.Usuario;
import br.com.controleleilao.negocio.ValidarLeilao;

public class ValidaLeilaoTeste {

	@Test
	public void testValidaNatureza() {
		assertEquals(ValidarLeilao.validaNatureza("DEMANDA"), true);
		assertEquals(ValidarLeilao.validaNatureza("OFERTA"), true);
		assertEquals(ValidarLeilao.validaNatureza("TESTE"), false);
		assertEquals(ValidarLeilao.validaNatureza(""), false);
	}

	@Test
	public void testValidaFormaLance() {
		assertEquals(ValidarLeilao.validaFormaLance("ABERTO"), true);
		assertEquals(ValidarLeilao.validaFormaLance("FECHADO"), true);
		assertEquals(ValidarLeilao.validaFormaLance("TESTE"), false);
		assertEquals(ValidarLeilao.validaFormaLance(""), false);
	}

	@Test
	public void testData() {
		assertEquals(ValidarLeilao.data(LocalDateTime.now(), LocalDateTime.now().plusDays(3)), null);
		assertEquals(ValidarLeilao.data(LocalDateTime.now(), LocalDateTime.now().minusMinutes(10)), null);
	}

	@Test
	public void testValidaVendedor() {
		Usuario vendedor = new Usuario();
		vendedor.setId(10);
		assertEquals(ValidarLeilao.validaVendedor(vendedor), true);
		vendedor.setId(0);
		assertEquals(ValidarLeilao.validaVendedor(vendedor), false);
	}

	@Test
	public void testValidaLotes() {
		ArrayList<Lote> lotes = new ArrayList<Lote>();
		Lote lote1 = new Lote(100);
		lotes.add(lote1);
		
		assertEquals(ValidarLeilao.validaLotes(lotes), true);
		assertEquals(ValidarLeilao.validaLotes(null), false);
	}

}
