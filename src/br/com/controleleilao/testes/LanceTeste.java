package br.com.controleleilao.testes;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.Test;

import br.com.controleleilao.negocio.Lance;
import br.com.controleleilao.negocio.Usuario;

public class LanceTeste {
	Lance lance = new Lance();
	@Test
	public void testLanceLocalDateTimeFloatUsuarioInt() {
		float valor = 10;
		Usuario user= new Usuario("Teste", "3333.2222.111-2", "email@teste.com");
		LocalDateTime agora = LocalDateTime.now();
		
		Lance lanceTeste = new Lance(agora, valor, user, 123);
		assertEquals(lanceTeste.getDataHoraLance(), agora);
		assertEquals(lanceTeste.getComprador().getNome(), user.getNome());
		assertEquals(lanceTeste.getIdLote(), 123);
		
	}

	@Test
	public void testLanceLocalDateTimeFloatUsuario() {
		float valor = 10;
		Usuario user= new Usuario("Teste", "3333.2222.111-2", "email@teste.com");
		LocalDateTime agora = LocalDateTime.now();
		
		Lance lanceTeste = new Lance(agora, valor, user);
		assertEquals(lanceTeste.getDataHoraLance(), agora);
		assertEquals(lanceTeste.getComprador().getNome(), user.getNome());
		
	}

	@Test
	public void testSetIdLance() {
		lance.setIdLance(123);
		assertEquals(lance.getIdLance(), 123);
	}

	@Test
	public void testSetDataHoraLance() {
		LocalDateTime agora = LocalDateTime.now();
		lance.setDataHoraLance(agora);
		assertEquals(lance.getDataHoraLance(), agora);
	}

	@Test
	public void testSetValor() {
		lance.setValor(10);
		assertEquals(lance.getValor(),10, 0.0000);
	}

	@Test
	public void testSetComprador() {
		Usuario user= new Usuario("Teste", "3333.2222.111-2", "email@teste.com");
		lance.setComprador(user);
		assertEquals(lance.getComprador().getCpfCnpj(), user.getCpfCnpj());
	}

	@Test
	public void testSetIdLote() {
		lance.setIdLote(12345);
		assertEquals(lance.getIdLote(),12345);
	}

}
