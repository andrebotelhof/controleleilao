package br.com.controleleilao.testes;

import static org.junit.Assert.*;

import org.junit.Test;

import br.com.controleleilao.negocio.Bem;
import br.com.controleleilao.negocio.BemException;
import br.com.controleleilao.negocio.BemFachada;
import br.com.controleleilao.negocio.ValidaBem;
import br.com.controleleilao.persistencia.BemDAO;
import br.com.controleleilao.persistencia.BemDAOException;

public class BemExceptionTeste {

	BemException teste = new BemException();

	@Test
	public void BemExceptionTeste() {
		BemException NovoTeste = new BemException("ERROR");
		assertEquals("ERROR", NovoTeste.getMessage());

	}
}
