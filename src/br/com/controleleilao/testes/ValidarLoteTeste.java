package br.com.controleleilao.testes;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import br.com.controleleilao.negocio.Bem;
import br.com.controleleilao.negocio.ValidarLote;

public class ValidarLoteTeste {

	/*@Test
	public void testValidaValor() {
		String valor = "10";
		assertEquals(ValidarLote.validaValor(valor), true);
		valor = "0";
		assertEquals(ValidarLote.validaValor(valor), false);
		//valor = "0";
		assertEquals(ValidarLote.validaValor(valor), false);
	}*/

	@Test
	public void testValidaBens() {
		ArrayList<Bem> bens = new ArrayList<Bem>();
		Bem bem1 = new Bem("Descri��o Breve", "Descri��o Completa", "Categoria");
		bens.add(bem1);
		
		assertEquals(ValidarLote.validaBens(bens), true);
		//assertEquals(ValidarLote.validaBens(null), false);
	}

}
