package br.com.controleleilao.testes;

import static org.junit.Assert.*;

import org.junit.Test;

import br.com.controleleilao.negocio.ValidaBem;

public class ValidaBemTeste {
	
	@Test
	public void testValidaDescricaoBreve() {
		assertEquals(ValidaBem.validaDescricaoBreve("DescricaoSemEspaco"), true);
		assertEquals(ValidaBem.validaDescricaoBreve(" "), false);
	}

	@Test
	public void testValidaDescricaoCompleta() {
		assertEquals(ValidaBem.validaDescricaoCompleta("DescricaoSemEspaco"), true);
		assertEquals(ValidaBem.validaDescricaoCompleta(" "), false);
	}

	@Test
	public void testValidaCategoria() {
		assertEquals(ValidaBem.validaCategoria("CategoriaSemEspaco"), true);
		assertEquals(ValidaBem.validaCategoria(" "), false);
	}

}
