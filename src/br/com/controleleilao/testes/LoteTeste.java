package br.com.controleleilao.testes;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import br.com.controleleilao.negocio.Bem;
import br.com.controleleilao.negocio.Lote;

public class LoteTeste {
	Lote lote = new Lote();
	@Test
	public void testLoteFloat() {
		Lote loteTeste = new Lote(10);
		assertEquals(loteTeste.getValor(), 10, 0.00);
	}

	@Test
	public void testLoteFloatArrayListOfBem() {
		float valor = 10;
		ArrayList<Bem> bens = new ArrayList<Bem>();
		Bem bem1 = new Bem("Descri��o Breve", "Descri��o Completa", "Categoria");
		bens.add(bem1);
		
		Lote loteTeste = new Lote(valor, bens);
		assertEquals(loteTeste.getValor(), valor, 0.00);
		assertEquals(loteTeste.getBens().get(0).getDescricaoBreve(), bem1.getDescricaoBreve());
	}

	@Test
	public void testSetId() {
		lote.setId(345);
		assertEquals(lote.getId(), 345);
	}

	@Test
	public void testSetValor() {
		float valor = 356;
		lote.setValor(valor);
		assertEquals(lote.getValor(), valor, 0.000);
	}

	@Test
	public void testSetBens() {
		ArrayList<Bem> bens = new ArrayList<Bem>();
		Bem bem1 = new Bem("Descri��o Breve1", "Descri��o Completa", "Categoria");
		Bem bem2 = new Bem("Descri��o Breve2", "Descri��o Completa", "Categoria");
		Bem bem3 = new Bem("Descri��o Breve3", "Descri��o Completa", "Categoria");
		bens.add(bem1);
		bens.add(bem2);
		bens.add(bem3);
		lote.setBens(bens);
		assertEquals(lote.getBens().get(0).getDescricaoBreve(), bem1.getDescricaoBreve());
		assertEquals(lote.getBens().get(1).getDescricaoBreve(), bem2.getDescricaoBreve());
		assertEquals(lote.getBens().get(2).getDescricaoBreve(), bem3.getDescricaoBreve());
	}

}
