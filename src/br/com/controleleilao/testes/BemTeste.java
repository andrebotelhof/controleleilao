package br.com.controleleilao.testes;

import static org.junit.Assert.*;

import org.junit.Test;

import br.com.controleleilao.negocio.Bem;

public class BemTeste {
	Bem bem = new Bem();

	@Test
	public void testBemStringStringString() {
		Bem bemTeste = new Bem("Descri��o breve", "Decri��o Completa", "Categoria");
		assertEquals("Descri��o breve", bemTeste.getDescricaoBreve());
		assertEquals("Decri��o Completa", bemTeste.getDescricaoCompleta());
		assertEquals("Categoria", bemTeste.getCategoria());
	}

	@Test
	public void testSetId() {
		bem.setId(110);
		assertEquals(110, bem.getId());
	}

	@Test
	public void testSetDescricaoBreve() {
		bem.setDescricaoBreve("Uma breve descri��o");
		assertEquals("Uma breve descri��o", bem.getDescricaoBreve());
	}

	@Test
	public void testSetDescricaoCompleta() {
		bem.setDescricaoCompleta("Uma  descri��o completa");
		assertEquals("Uma  descri��o completa", bem.getDescricaoCompleta());
	}

	@Test
	public void testSetCategoria() {
		bem.setCategoria("Categoria m�veis");
		assertEquals("Categoria m�veis", bem.getCategoria());
	}

}
