package br.com.controleleilao.testes;

import static org.junit.Assert.*;

import org.junit.Test;

import br.com.controleleilao.negocio.Usuario;

public class UsuarioTeste {
	Usuario user = new Usuario();
	
	@Test
	public void testUsuarioStringStringString() {
		String nome = "teste";
		String cpf = "333.3333.333-3";
		String email = "email@teste.com";
		Usuario userTeste = new Usuario(nome, cpf, email);
		
		assertEquals(userTeste.getNome(), nome);
		assertEquals(userTeste.getCpfCnpj(), cpf);
		assertEquals(userTeste.getEmail(), null);
	}

	@Test
	public void testSetId() {
		user.setId(90);
		assertEquals(user.getId(), 90);
	}

	@Test
	public void testSetNome() {
		user.setNome("Teste");
		assertEquals(user.getNome(), "Teste");
	}

	@Test
	public void testSetCpf_cnpj() {
		user.setCpf_cnpj("3333.3333.333-3");
		assertEquals(user.getCpfCnpj(), "3333.3333.333-3");
		
	}

	@Test
	public void testSetEmail() {
		user.setEmail("email@teste.com");
		assertEquals(user.getEmail(), "email@teste.com");
	}

}
