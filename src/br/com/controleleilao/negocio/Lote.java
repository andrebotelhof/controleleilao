package br.com.controleleilao.negocio;

import java.util.ArrayList;

public class Lote {
	private int id;
	private float valor;
	private ArrayList<Bem> bens;
	
	public Lote() {
	}
	
	public Lote(float valor) {
		this.valor=valor;
	}
	
	public Lote(float valor, ArrayList<Bem> bens) {
		this.valor=valor;
		this.bens=bens;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public float getValor() {
		return valor;
	}
	
	public void setValor(Float valor) {
		this.valor = valor;
	}
	
	public ArrayList<Bem> getBens() {
		return bens;
	}
	
	public void setBens(ArrayList<Bem> bens) {
		this.bens = bens;
	}
	
	
}
