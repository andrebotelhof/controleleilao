package br.com.controleleilao.negocio;

import br.com.controleleilao.persistencia.UsuarioDAO;
import br.com.controleleilao.persistencia.UsuarioDAOException;

public class UsuarioFachada {
	private UsuarioDAO usuarioDAO = new UsuarioDAO();;
	
	public UsuarioFachada() {
	}
	
	
	public Usuario cadastraUsuario(String nome, String cpfCNPJ, String email) throws UsuarioException{
        if(!ValidarUsuario.validaNome(nome)) {
            throw new UsuarioException("Nome do usuario invalido!");
        }
        if(!ValidarUsuario.validaCpfCnPJ(cpfCNPJ)) {
            throw new UsuarioException("CPF/CNPJ do usuario invalido!");
        }
        if(!ValidarUsuario.validaEmail(email)) {
            throw new UsuarioException("E-mail do usuario invalido!");
        }
        Usuario user = new Usuario(nome, cpfCNPJ, email);
        try {
            int id = usuarioDAO.cadastraUsuario(user);
            if (id > 0) {
            	user.setId(id);
            	return user;
            } else {
            	return null;
            }
        } catch (UsuarioDAOException e) {
            throw new UsuarioException("Erro para cadastrar o usuario!", e);
        }
    }
	
	public Usuario loginUsuario(String email, String cpfCnpj) throws UsuarioException {
		 try {
	            Usuario user = usuarioDAO.loginUsuario(email, cpfCnpj);
	            return user;
	            
	        } catch (UsuarioDAOException e) {
	            throw new UsuarioException("Usuario nao cadastrado!", e);
	        }
	}
}
