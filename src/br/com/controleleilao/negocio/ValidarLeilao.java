package br.com.controleleilao.negocio;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class ValidarLeilao {
	
	public static boolean validaNatureza(String natureza) {
		return (natureza.equals("DEMANDA") || natureza.equals("OFERTA"));
	}
	
	public static boolean validaFormaLance(String formaLance) {
		return (formaLance.equals("ABERTO") || formaLance.equals("FECHADO"));
	}
	
	public static boolean validaData(String anoInicio, String mesInicio, 
			String diaInicio, String horaInicio, String minutoInicio) throws LeilaoException {
		try {
			Integer anoFloat = Integer.valueOf(anoInicio);
			Integer mesFloat = Integer.valueOf(mesInicio);
			Integer diaFloat = Integer.valueOf(diaInicio);
			Integer horaFloat = Integer.valueOf(horaInicio);
			Integer minutoFloat = Integer.valueOf(minutoInicio);
			if (mesFloat > 12 || mesFloat <= 0)
				return false;
			if (diaFloat > 31 || diaFloat < 0)
				return false;
			if (horaFloat > 24 || horaFloat < 0)
				return false;
			if (minutoFloat > 60 || minutoFloat < 0)
				return false;
			return true;
		} catch (NumberFormatException e) {
			throw new LeilaoException("Valor invalido!");
		}
	}
	
	public static boolean validaDatasAfter(LocalDateTime dataInicio, LocalDateTime dataFim) {
		if(dataInicio.isAfter(dataFim))
			return false;
		else
			return true;
	}
	
	public static boolean validaVendedor(Usuario vendedor) {
		if (vendedor.getId() > 0)
			return true;
		else
			return false;
	}
	
	public static boolean validaLotes(ArrayList<Lote> lotes) {
		if (lotes == null)
			return false;
		else
			return true;
	}

	public static Object data(LocalDateTime now, LocalDateTime plusDays) {
		// TODO Auto-generated method stub
		return null;
	}
}
