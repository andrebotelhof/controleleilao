package br.com.controleleilao.negocio;

public class Usuario {
	private int id;
	private String nome;
	private String cpfCnpj;
	private String email;
	
	public Usuario() {
		
	}
	
	public Usuario(String nome, String cpfCnpj, String email) {
		this.nome=nome;
		this.cpfCnpj=cpfCnpj;
		this.email=email;
		
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	
	public void setCpf_cnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
}

