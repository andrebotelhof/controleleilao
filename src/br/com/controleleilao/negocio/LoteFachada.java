package br.com.controleleilao.negocio;

import java.time.LocalDateTime;
import java.util.ArrayList;

import br.com.controleleilao.persistencia.BemDAOException;
import br.com.controleleilao.persistencia.LoteDAO;
import br.com.controleleilao.persistencia.LoteDAOException;
import br.com.controleleilao.persistencia.LoteDAO;
import br.com.controleleilao.persistencia.LoteDAOException;

public class LoteFachada {
	private LoteDAO loteDAO = new LoteDAO();
	
	public Lote cadastraLote(Lote lote, int idLeilao) throws LoteException, BemException{
        try {
            int id = loteDAO.cadastraLote(lote, idLeilao);
            BemFachada bemFachada = new BemFachada();
            if (lote.getBens() != null) {
            	for (Bem bem : lote.getBens()) {
            		bemFachada.cadastraBem(bem, id);
            	}
            }
            if (id > 0) {
            	lote.setId(id);
            	return lote;
            } else {
            	return null;
            }
        } catch (LoteDAOException e) {
            throw new LoteException("Erro para cadastrar o usuario!", e);
        }
    }
	
	public boolean validaLote(String valor, ArrayList<Bem> bens) throws LoteException{
        if(!ValidarLote.validaValor(valor)) {
            throw new LoteException("Valor invalido!");
        }
        if(!ValidarLote.validaBens(bens)) {
            throw new LoteException("Adicione pelo menos um bem!");
        }
        return true;
    }
}
