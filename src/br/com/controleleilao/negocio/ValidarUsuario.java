package br.com.controleleilao.negocio;

public class ValidarUsuario {
	
	//Mais infantil que todos abaixo
	public static boolean validaNome(String nome) {
		return (nome.contains(" "));
	}
	
	//Infantil, tenta melhora
	public static boolean validaCpfCnPJ(String cpfCNPJ) {
		char[] c = cpfCNPJ.toCharArray();
		if (cpfCNPJ.length()==11 || cpfCNPJ.length()==14) {
			for ( int i = 0; i < c.length; i++ ) {  
				if (!Character.isDigit(c[i]))
					return false;
			}
			return true;
		} else {
			return false;
		}
	}
	
	//Mais infantil ainda, tenta melhora mais o/
	public static boolean validaEmail(String email) {
		return (email.contains("@") && email.contains("."));
	}
}
