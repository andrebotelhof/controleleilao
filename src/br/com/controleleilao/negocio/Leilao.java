package br.com.controleleilao.negocio;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Leilao {
	private int id;
	private String natureza;
	private String formaLance;
	private LocalDateTime dataHoraInicio;
	private LocalDateTime dataHoraFim;
	private Usuario vendedor;
	private ArrayList<Lote> lotes;
	
	public Leilao() {
	}
	
	public Leilao(String natureza, String formaLance, LocalDateTime dataHoraInicio, LocalDateTime dataHoraFim, Usuario vendedor) {
		this.natureza=natureza;
		this.formaLance=formaLance;
		this.dataHoraInicio=dataHoraInicio;
		this.dataHoraFim=dataHoraFim;
		this.vendedor=vendedor;
	}
	
	public Leilao(String natureza, String formaLance, LocalDateTime dataHoraInicio, LocalDateTime dataHoraFim, Usuario vendedor, ArrayList<Lote> lotes) {
		this.natureza=natureza;
		this.formaLance=formaLance;
		this.dataHoraInicio=dataHoraInicio;
		this.dataHoraFim=dataHoraFim;
		this.vendedor=vendedor;
		this.lotes=lotes;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNatureza() {
		return natureza;
	}
	
	public void setNatureza(String natureza) {
		this.natureza = natureza;
	}
	
	public String getFormaLance() {
		return formaLance;
	}
	
	public void setFormaLance(String formaLance) {
		this.formaLance = formaLance;
	}
	
	public LocalDateTime getDataHoraInicio() {
		return dataHoraInicio;
	}
	
	public void setDataHoraInicio(LocalDateTime dataHoraInicio) {
		this.dataHoraInicio = dataHoraInicio;
	}
	
	public LocalDateTime getDataHoraFim() {
		return dataHoraFim;
	}
	
	public void setDataHoraFim(LocalDateTime dataHoraFim) {
		this.dataHoraFim = dataHoraFim;
	}
	
	public Usuario getVendedor() {
		return vendedor;
	}
	
	public void setVendedor(Usuario vendedor) {
		this.vendedor = vendedor;
	}

	public ArrayList<Lote> getLotes() {
		return lotes;
	}

	public void setLotes(ArrayList<Lote> lotes) {
		this.lotes = lotes;
	}
}
