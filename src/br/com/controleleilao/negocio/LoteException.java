package br.com.controleleilao.negocio;

public class LoteException extends Exception {
	public LoteException() {
    }

    public LoteException(String msg) {
        super(msg);
    }

    public LoteException(String message, Throwable cause) {
        super(message, cause);
    }
}
