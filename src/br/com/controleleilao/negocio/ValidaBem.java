package br.com.controleleilao.negocio;

public class ValidaBem {
	public static boolean validaDescricaoBreve(String descricaoBreve) {
		if (descricaoBreve.equals(" ") || descricaoBreve.equals(""))
			return false;
		else
			return true;
	}
	
	public static boolean validaDescricaoCompleta(String descricaoCompleta) {
		if (descricaoCompleta.equals(" ") || descricaoCompleta.equals(""))
			return false;
		else
			return true;
	}
	
	public static boolean validaCategoria(String categoria) {
		if (categoria.equals(" ") || categoria.equals(""))
			return false;
		else
			return true;
	}
}
