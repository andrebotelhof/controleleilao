package br.com.controleleilao.negocio;

public class LeilaoException extends Exception {
	public LeilaoException() {
    }

    public LeilaoException(String msg) {
        super(msg);
    }

    public LeilaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
