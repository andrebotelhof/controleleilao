package br.com.controleleilao.negocio;

public class LanceException extends Exception {
	public LanceException() {
    }

    public LanceException(String msg) {
        super(msg);
    }

    public LanceException(String message, Throwable cause) {
        super(message, cause);
    }
}
