package br.com.controleleilao.negocio;

import java.time.LocalDateTime;
import java.util.ArrayList;

import br.com.controleleilao.persistencia.BemDAOException;
import br.com.controleleilao.persistencia.LeilaoDAO;
import br.com.controleleilao.persistencia.LeilaoDAOException;
import br.com.controleleilao.persistencia.LoteDAOException;
import br.com.controleleilao.persistencia.UsuarioDAO;
import br.com.controleleilao.persistencia.UsuarioDAOException;

public class LeilaoFachada {
	
	private LeilaoDAO leilaoDAO = new LeilaoDAO();
	
	public Leilao cadastraLeilao(Leilao leilao) throws LeilaoException, LoteException, BemException{	
      //  Leilao leilao = new Leilao(natureza, formaLance, dataHoraInicio, dataHoraFim, vendedor);
        try {
            int id = leilaoDAO.cadastraLeilao(leilao);
            LoteFachada loteFachada = new LoteFachada();
            for (Lote lote : leilao.getLotes()) {
            	if (lote.getBens() != null)
            		loteFachada.cadastraLote(lote, id);
            }
            if (id > 0) {
            	leilao.setId(id);
            	return leilao;
            } else {
            	return null;
            }
        } catch (LeilaoDAOException e) {
            throw new LeilaoException("Erro para cadastrar o leilao!", e);
        }
    }
	
	public boolean validaLeilao(String natureza, String formaLance, String anoInicio, String mesInicio, 
			String diaInicio, String horaInicio, String minutoInicio, String anoFim, String mesFim, String diaFim, 
			String horaFim, String minutoFim, Usuario vendedor) throws LeilaoException{	
		if(!ValidarLeilao.validaNatureza(natureza)) {
            throw new LeilaoException("Natureza invalida!");
        }
        if(!ValidarLeilao.validaFormaLance(formaLance)) {
            throw new LeilaoException("Forma de lance invalido!");
        }
        
        if(!ValidarLeilao.validaData(anoInicio, mesInicio, diaInicio, horaInicio, minutoInicio)) {
            throw new LeilaoException("Data de inicio do leilao invalida!");
        }
        if(!ValidarLeilao.validaData(anoFim, mesFim, diaFim, horaFim, minutoFim)) {
            throw new LeilaoException("Data de fim do leilao invalida!");
        }
        
        LocalDateTime dataInicio = LocalDateTime.of(Integer.valueOf(anoInicio).intValue(), 
        		Integer.valueOf(mesInicio).intValue(), Integer.valueOf(diaInicio).intValue(), 
        		Integer.valueOf(horaInicio).intValue(), Integer.valueOf(minutoInicio).intValue());
        
        LocalDateTime dataFim = LocalDateTime.of(Integer.valueOf(anoFim).intValue(), 
        		Integer.valueOf(mesFim).intValue(), Integer.valueOf(diaFim).intValue(), 
        		Integer.valueOf(horaFim).intValue(), Integer.valueOf(minutoFim).intValue());
        
        if(!ValidarLeilao.validaDatasAfter(dataInicio, dataFim)) {
            throw new LeilaoException("A data de inicio nao pode ser superior a data de fim!");
        }
        if(!ValidarLeilao.validaVendedor(vendedor)) {
            throw new LeilaoException("Responsavel invalido!");
        }
        return true;
    }
	
	public ArrayList<Leilao> listLeiloes() throws LeilaoException, LoteException, BemException {
		ArrayList<Leilao> leiloes = new ArrayList<Leilao>();
		try {
				leiloes =  leilaoDAO.listaLeiloes();
				return leiloes;
		} catch (LeilaoDAOException e) {
            throw new LeilaoException("Erro para cadastrar o usuario!", e);
        } catch (LoteDAOException e) {
            throw new LoteException("Erro para cadastrar o usuario!", e);
        } catch (BemDAOException e) {
            throw new BemException("Erro para cadastrar o usuario!", e);
        }
	}
}
