package br.com.controleleilao.negocio;

import java.time.LocalDateTime;
import java.util.ArrayList;

import br.com.controleleilao.persistencia.LanceDAO;
import br.com.controleleilao.persistencia.LanceDAOException;

public class LanceFachada {
	private LanceDAO lanceDAO = new LanceDAO();;
	public Lance cadastraLance(LocalDateTime dataHoraLance, String valor, Usuario comprador, int idLote) throws LanceException{
        if(!ValidaLance.validaDataHora(dataHoraLance)) {
            throw new LanceException("Data/Hora do lance invalido!");
        }
        if(!ValidaLance.validaValor(valor)) {
            throw new LanceException("Valor invalido!");
        }
        float valorFloat = Float.valueOf(valor);
        if(!ValidaLance.validaUsuario(comprador)) {
            throw new LanceException("Comprador invalido!");
        }
        if(!ValidaLance.validaLote(idLote)) {
            throw new LanceException("Lote invalido!");
        } 
        Lance lance = new Lance(dataHoraLance, valorFloat, comprador, idLote);
        try {
            int id = lanceDAO.cadastraLance(lance);
            if (id > 0) {
            	lance.setIdLance(id);
            	return lance;
            } else {
            	return null;
            }
        } catch (LanceDAOException e) {
            throw new LanceException("Erro para cadastrar o lance!", e);
        }
    }
	
	public ArrayList<Lance> listaLancesPorLote(int idLote) throws LanceException {
		try {
			ArrayList<Lance> lances = lanceDAO.listaLancesPorLote(idLote);
			return lances;
		} catch (LanceDAOException e) {
			throw new LanceException("Erro para listar os lancess!", e);
		}
	}
	
	public void cancelaLance(int idLance) throws LanceException {
		try {
			boolean ver = lanceDAO.cancelaLance(idLance);
		} catch (LanceDAOException e) {
			throw new LanceException("Erro para cancelar os lances", e);
		}
	}
}
