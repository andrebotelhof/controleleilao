package br.com.controleleilao.negocio;

import java.util.ArrayList;

public class ValidarLote {
	private static boolean validaValorMaior(float valor) {
		if (valor > 0)
			return true;
		else
			return false;
	}
	
	public static boolean validaBens(ArrayList<Bem> bens) {
		if (bens.size() == 0)
			return false;
		else
			return true;
	}
	
	public static boolean validaValor(String valor) throws LoteException{
        try {
        	float valorFloat = Float.valueOf(valor);
        	return validaValorMaior(valorFloat);
        } catch(NumberFormatException e) {
        	throw new LoteException("Valor invalido!");
        }
    }
	
}
