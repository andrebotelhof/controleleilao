package br.com.controleleilao.negocio;

public class BemException extends Exception {
	public BemException() {
    }

    public BemException(String msg) {
        super(msg);
    }

    public BemException(String message, Throwable cause) {
        super(message, cause);
    }
}
