package br.com.controleleilao.negocio;

import java.time.LocalDateTime;

public class Lance {
	private int idLance;
	private LocalDateTime dataHoraLance;
	private float valor;
	private Usuario comprador;
	private int idLote;
	
	public Lance() {
	}
	
	public Lance(LocalDateTime dataHoraLance, float valor, Usuario comprador, int idLote) {
		this.dataHoraLance=dataHoraLance;
		this.valor=valor;
		this.comprador=comprador;
		this.idLote=idLote;
	}
	
	public Lance(LocalDateTime dataHoraLance, float valor, Usuario comprador) {
		this.dataHoraLance=dataHoraLance;
		this.valor=valor;
		this.comprador=comprador;
	}
	
	public int getIdLance() {
		return idLance;
	}
	
	public void setIdLance(int idLance) {
		this.idLance = idLance;
	}
	
	public LocalDateTime getDataHoraLance() {
		return dataHoraLance;
	}
	
	public void setDataHoraLance(LocalDateTime dataHoraLance) {
		this.dataHoraLance = dataHoraLance;
	}
	
	public float getValor() {
		return valor;
	}
	
	public void setValor(float valor) {
		this.valor = valor;
	}
	
	public Usuario getComprador() {
		return comprador;
	}
	
	public void setComprador(Usuario comprador) {
		this.comprador = comprador;
	}
	
	public int getIdLote() {
		return idLote;
	}

	public void setIdLote(int idLote) {
		this.idLote = idLote;
	}
	
	public String toString() {
		return "Data e hora: " + dataHoraLance.toString() + " Valor: R$ " + String.valueOf(valor) + " Id. do usuario " + comprador.getId();
	}
}
