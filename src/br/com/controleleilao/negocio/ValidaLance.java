package br.com.controleleilao.negocio;

import java.time.LocalDateTime;

public class ValidaLance {
	
	public static boolean validaValor(String valor) throws LanceException {
		try {
		float valorFloat = Float.valueOf(valor);
		if (valorFloat > 0)
			return true;
		else
			return false;
		} catch (NumberFormatException e) {
			throw new LanceException("Valor invalido!");
		}
	}
	
	//Trabalha melhor nesse
	public static boolean validaDataHora(LocalDateTime dataHora) {
		return true;
	}
	
	public static boolean validaUsuario(Usuario user) {
		if (user.getId() > 0)
			return true;
		else
			return false;
	}
	
	public static boolean validaLote(int idLote) {
		if (idLote > 0)
			return true;
		else
			return false;
	}
}
