package br.com.controleleilao.negocio;

import java.time.LocalDateTime;
import java.util.ArrayList;

import br.com.controleleilao.persistencia.BemDAO;
import br.com.controleleilao.persistencia.BemDAOException;

public class BemFachada {
	private BemDAO bemDAO = new BemDAO();
	
	public Bem cadastraBem(Bem bem, int idLote) throws BemException{
        try {
            int id = bemDAO.cadastraBem(bem, idLote);
            if (id > 0) {
            	bem.setId(id);
            	return bem;
            } else {
            	return null;
            }
        } catch (BemDAOException e) {
            throw new BemException("Erro para cadastrar o bem!", e);
        }
    }
	
	public boolean validaBem(String descricaoBreve, String descricaoCompleta, String categoria) throws BemException{
        if(!ValidaBem.validaDescricaoBreve(descricaoBreve)) {
            throw new BemException("Descricao breve invalida!");
        }
        if(!ValidaBem.validaDescricaoCompleta(descricaoCompleta)) {
            throw new BemException("Descricao completa invalida!");
        }
        if(!ValidaBem.validaCategoria(categoria)) {
            throw new BemException("Categoria invalida!");
        }
        return true;
	}
}
