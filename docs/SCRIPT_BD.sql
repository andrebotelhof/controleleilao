-- Criacao do BD

CREATE TABLE usuario (
id_usuario INT(11) PRIMARY KEY AUTO_INCREMENT,
nome VARCHAR(100) NOT NULL,
cpf_cnpj VARCHAR(100) NOT NULL,
email VARCHAR(100) NOT NULL
) Engine=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

CREATE TABLE leilao (
id_leilao INT(11) PRIMARY KEY AUTO_INCREMENT,
natureza VARCHAR(100) NOT NULL,
forma_lance VARCHAR(100) NOT NULL,
data_hora_inicio DATETIME NOT NULL,
data_hora_fim DATETIME NOT NULL,
id_vendedor INT(11) NOT NULL,
FOREIGN KEY(id_vendedor) REFERENCES usuario (id_usuario)
) Engine=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

CREATE TABLE bem (
id_bem INT(11) PRIMARY KEY AUTO_INCREMENT,
descricao_breve VARCHAR(100) NOT NULL,
descricao_completa VARCHAR(100) NOT NULL,
categoria VARCHAR(100) NOT NULL
) Engine=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

CREATE TABLE lote (
id_lote INT(11) PRIMARY KEY AUTO_INCREMENT,
id_leilao INT(11) NOT NULL,
valor FLOAT(11,2) NOT NULL,
FOREIGN KEY(id_leilao) REFERENCES leilao (id_leilao)
) Engine=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

CREATE TABLE lote_bem (
id_lote_bem INT(11) PRIMARY KEY AUTO_INCREMENT,
id_bem INT(11) NOT NULL,
id_lote INT(11) NOT NULL,
FOREIGN KEY(id_bem) REFERENCES bem (id_bem),
FOREIGN KEY(id_lote) REFERENCES lote (id_lote)
) Engine=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

CREATE TABLE lance (
id_lance INT(11) PRIMARY KEY AUTO_INCREMENT,
data_hora_lance DATETIME NOT NULL,
valor VARCHAR(10) NOT NULL,
id_usuario INT(11) NOT NULL,
id_lote INT(11) NOT NULL,
FOREIGN KEY(id_usuario) REFERENCES usuario (id_usuario),
FOREIGN KEY(id_lote) REFERENCES lote (id_lote)
) Engine=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Inserts no BD

INSERT INTO usuario (nome, cpf_cnpj, email) VALUES ("Andre Botelho", "123456789", "andrebotelho@live.com");
INSERT INTO usuario (nome, cpf_cnpj, email) VALUES ("Matheus Bondan", "987654321", "matheusbondan@live.com");
INSERT INTO usuario (nome, cpf_cnpj, email) VALUES ("Usuario comprador", "123446789", "comprador1@live.com");
INSERT INTO usuario (nome, cpf_cnpj, email) VALUES ("Usuario comprador2", "122446789", "comprador1@live.com");

INSERT INTO leilao (natureza, forma_lance, data_hora_inicio, data_hora_fim, id_vendedor) VALUES ("OFERTA", "ABERTO", "2017-01-01 01:00:00", "2018-01-01 01:00:00", 10);
INSERT INTO leilao (natureza, forma_lance, data_hora_inicio, data_hora_fim, id_vendedor) VALUES ("DEMANDA", "ABERTO", "2017-01-01 01:00:00", "2018-01-01 01:00:00", 11);
INSERT INTO leilao (natureza, forma_lance, data_hora_inicio, data_hora_fim, id_vendedor) VALUES ("DEMANDA", "FECHADO", "2017-01-01 01:00:00", "2018-01-01 01:00:00", 10);
INSERT INTO leilao (natureza, forma_lance, data_hora_inicio, data_hora_fim, id_vendedor) VALUES ("OFERTA", "FECHADO", "2017-01-01 01:00:00", "2018-01-01 01:00:00", 11);

INSERT INTO lote (id_leilao, valor) VALUES (10, 1008.52);
INSERT INTO lote (id_leilao, valor) VALUES (10, 1058.52);
INSERT INTO lote (id_leilao, valor) VALUES (11, 108.52);
INSERT INTO lote (id_leilao, valor) VALUES (11, 8.52);
INSERT INTO lote (id_leilao, valor) VALUES (12, 18.52);
INSERT INTO lote (id_leilao, valor) VALUES (12, 10.52);
INSERT INTO lote (id_leilao, valor) VALUES (13, 1.52);
INSERT INTO lote (id_leilao, valor) VALUES (13, 0.52);

INSERT INTO bem (descricao_breve, descricao_completa, categoria) VALUES ("Fiat Palio","Fiat Palio Completo 1.0","AUTOMOVEL");
INSERT INTO bem (descricao_breve, descricao_completa, categoria) VALUES ("Fiat Uno","Deus das estradas!","AUTOMOVEL");
INSERT INTO bem (descricao_breve, descricao_completa, categoria) VALUES ("Casa","Alvenaria Completa ","IMOVEL");
INSERT INTO bem (descricao_breve, descricao_completa, categoria) VALUES ("Apartamento","Apartamento de madeira serio","IMOVEL");
INSERT INTO bem (descricao_breve, descricao_completa, categoria) VALUES ("Colar","Colar de ouro quilates","JOIA");
INSERT INTO bem (descricao_breve, descricao_completa, categoria) VALUES ("Anel","Anel de casamento","JOIA");
INSERT INTO bem (descricao_breve, descricao_completa, categoria) VALUES ("Pulseira","De pedra que da duas voltas","JOIA");
INSERT INTO bem (descricao_breve, descricao_completa, categoria) VALUES ("Armario","Armario setenta portas","MOVEL");
INSERT INTO bem (descricao_breve, descricao_completa, categoria) VALUES ("Estante","Preta tri bonita","MOVEL");
INSERT INTO bem (descricao_breve, descricao_completa, categoria) VALUES ("Cama","Cama de casal","MOVEL");

INSERT INTO lote_bem (id_bem, id_lote) VALUES (10, 10);
INSERT INTO lote_bem (id_bem, id_lote) VALUES (11, 11);
INSERT INTO lote_bem (id_bem, id_lote) VALUES (12, 12);
INSERT INTO lote_bem (id_bem, id_lote) VALUES (13, 13);
INSERT INTO lote_bem (id_bem, id_lote) VALUES (14, 14);
INSERT INTO lote_bem (id_bem, id_lote) VALUES (15, 15);
INSERT INTO lote_bem (id_bem, id_lote) VALUES (16, 16);
INSERT INTO lote_bem (id_bem, id_lote) VALUES (17, 17);
INSERT INTO lote_bem (id_bem, id_lote) VALUES (18, 12);
INSERT INTO lote_bem (id_bem, id_lote) VALUES (19, 15);

INSERT INTO lance (data_hora_lance, valor, id_usuario, id_lote) VALUES ("2017-06-06 01:00:00", 1234.54, 12, 10);
INSERT INTO lance (data_hora_lance, valor, id_usuario, id_lote) VALUES ("2017-06-06 01:00:00", 543.54, 12, 10);
INSERT INTO lance (data_hora_lance, valor, id_usuario, id_lote) VALUES ("2017-06-06 01:00:00", 345.21, 13, 11);
INSERT INTO lance (data_hora_lance, valor, id_usuario, id_lote) VALUES ("2017-06-06 01:00:00", 321.65, 10, 12);
